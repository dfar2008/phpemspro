# 运行环境
PHP 7.0+  
MYSQL 5.6+  
REDIS  
# 安装步骤
1、安装php7.0和mysql，我使用的是lnmp1.5
2、安装redis，lnmp里插件可以直接安装 ./addons.sh install redis 
3、查看phpinfo确认redis是否安装，通过top命令查看redis-server是否安装好，也可以到redis的安装目录下的bin目录执行redis-cli确认redis是否运行正常
4、chmod 777 -R public/data 目录，否则进入index.php页面将出现白屏
5、修改public\lib\config.cls.php,设置主从数据库的信息，redis的信息默认即可
5、即可使用默认的管理员登录系统，管理员的个人中心可以进入后台管理

# 项目中运用到的开源软件或SDK
CKEDITOR  
CKPLAYER  
Bootstrap3  
Jquery  
FineUploader  
Swiper3  
微信支付官方sdk  
支付宝支付官方sdk  

# 默认管理员用户名和密码
peadmin 222222